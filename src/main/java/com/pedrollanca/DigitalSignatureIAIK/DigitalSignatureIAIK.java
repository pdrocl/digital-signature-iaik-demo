package com.pedrollanca.DigitalSignatureIAIK;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.util.Enumeration;

import iaik.asn1.structures.AlgorithmID;
import iaik.pkcs.PKCSException;
import iaik.pkcs.pkcs7.ContentInfo;
import iaik.pkcs.pkcs7.IssuerAndSerialNumber;
import iaik.pkcs.pkcs7.SignedData;
import iaik.pkcs.pkcs7.SignerInfo;
import iaik.security.provider.IAIK;
import iaik.security.ssl.SecurityProvider;
import iaik.utils.Base64OutputStream;

public class DigitalSignatureIAIK {

	private static String PATH_TO_KEYSTORE;
	private static String KEY_ALIAS_IN_KEYSTORE;
	private static String KEYSTORE_PASSWORD;

	public DigitalSignatureIAIK(String path, String alias, String password) {
		PATH_TO_KEYSTORE = path;
		KEY_ALIAS_IN_KEYSTORE = alias;
		KEYSTORE_PASSWORD = password;
		Security.addProvider(new IAIK());
	}

	public static String sign(String msg) throws KeyStoreException, NoSuchAlgorithmException, CertificateException,
			IOException, PKCSException, UnrecoverableKeyException {

		KeyStore keystore = KeyStore.getInstance("PKCS12");
		InputStream is = new FileInputStream(PATH_TO_KEYSTORE);
		keystore.load(is, KEYSTORE_PASSWORD.toCharArray());

		boolean isCertificateLoaded = false;
		Enumeration enumeration = keystore.aliases();

		while (enumeration.hasMoreElements())
			if (enumeration.nextElement().toString().equals(KEY_ALIAS_IN_KEYSTORE)) {
				isCertificateLoaded = true;
				break;
			}
		
		if (!isCertificateLoaded)
			throw new KeyStoreException("IAIK: Empty Keystore.");

		java.security.cert.Certificate certificate = keystore.getCertificate(KEY_ALIAS_IN_KEYSTORE);

		PrivateKey privatekey = (PrivateKey) keystore.getKey(KEY_ALIAS_IN_KEYSTORE, KEYSTORE_PASSWORD.toCharArray());

		iaik.x509.X509Certificate ax509certificate[] = new iaik.x509.X509Certificate[1];
		ax509certificate[0] = new iaik.x509.X509Certificate(certificate.getEncoded());

		byte aByteInfoToSign[] = msg.getBytes("UTF8");

		IssuerAndSerialNumber issuerandserialnumber = new IssuerAndSerialNumber(ax509certificate[0]);

		SignerInfo asignerinfo[] = new SignerInfo[1];
		asignerinfo[0] = new SignerInfo(issuerandserialnumber, AlgorithmID.sha1, AlgorithmID.rsaEncryption,
				(java.security.PrivateKey) privatekey);

		SecurityProvider.setSecurityProvider(new SecurityProvider());

		SignedData signeddata = new SignedData(aByteInfoToSign, SignedData.IMPLICIT);
		signeddata.setCertificates(ax509certificate);
		signeddata.addSignerInfo(asignerinfo[0]);

		ContentInfo contentinfo = new ContentInfo(signeddata);

		ByteArrayOutputStream result = new ByteArrayOutputStream();
		ByteArrayOutputStream source = new ByteArrayOutputStream();

		contentinfo.writeTo(source);

		Base64OutputStream base64outputstream = new Base64OutputStream(result);
		base64outputstream.write(source.toByteArray());
		base64outputstream.flush();
		base64outputstream.close();

		String resFinal;
		resFinal = result.toString().replaceAll("[\r\n]+", "");
		return resFinal;

	}

}
