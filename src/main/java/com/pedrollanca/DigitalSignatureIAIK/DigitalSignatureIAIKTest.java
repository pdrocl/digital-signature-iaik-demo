package com.pedrollanca.DigitalSignatureIAIK;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import iaik.pkcs.PKCSException;
import junit.framework.TestCase;

public class DigitalSignatureIAIKTest extends TestCase {
	DigitalSignatureIAIK ds;
	String message="Test plain message";
	protected void setUp() throws Exception {
		ds=new DigitalSignatureIAIK("test.p12","privatekey123","123456");
		super.setUp();
	}

	public void testDigitalSignatureIAIK() {
		assertNotNull(ds);
	}

	public void testSign() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, PKCSException {
		String signedData=ds.sign(message);
		assertNotNull(signedData);
		System.out.println(signedData);
	}

}
